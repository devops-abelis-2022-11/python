#!/bin/bash

if [[ ! -v DJANGO_SUPERUSER_PASSWORD ]]; then
  echo "ERROR, missing DJANGO_SUPERUSER_PASSWORD env" 1&>2
  exit 1
fi

python manage.py createsuperuser --username admin --noinput

python manage.py runserver 0.0.0.0:8000
